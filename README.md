# cmus controller library

The cmus controller library contains utilities to control the cmus player from python.

# Requirements:
1.  cmus must be installed

see: https://cmus.github.io/#documentation

2.  screen must be installed 

on debian can be installed with apt-get install screen

for other distribution check https://www.gnu.org/software/screen/