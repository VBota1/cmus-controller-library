# MIT License
#
# Copyright (c) 2019 V Bota
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Version API.Functionality.Patch
# Version 1.0.0

import subprocess
import time

class PlayerStatus():
    '''
    Defines the states that the cmus player can have.
    '''
    unknown = 'unknown'
    not_running = 'is not running'
    playing = 'playing'
    stopped = 'stopped'

def get_music_player_state():
    '''
    Reads and returns the cmus player state.
    Returns:
        status (str): The status of the cmus player as one of the values defined in PlayerStatus
        position (str): The position of the currently playing song or '0'
    '''
    status = PlayerStatus.unknown
    position = '0'

    cmus_status = subprocess.run(['cmus-remote','-Q'], stdout=subprocess.PIPE)
    if cmus_status is not None and cmus_status.stdout is not None:
        lines = cmus_status.stdout.decode().splitlines()
        if 0 == len(lines):
            status = PlayerStatus.not_running
        for line in lines:
            if 'status' in line:
                if PlayerStatus.playing in line:
                    status = PlayerStatus.playing
                if PlayerStatus.stopped in line:
                    status = PlayerStatus.stopped
            elif 'position' in line:
                position = line.replace('position ','')
                break

    return status, position

def run_music_player(max_time_to_wait_for_start):
    '''
    Starts the cmus player in a detached screen session.
    Parameters:
        max_time_to_wait_for_start (number): maximum number of seconds to wait until cmus is up and running
    '''
    subprocess.Popen(['screen','-dmS','cmusScreen','cmus'])
    if max_time_to_wait_for_start is not None:
        start = time.time()
        while max_time_to_wait_for_start > time.time() - start:
            last_player_status, position = get_music_player_state()
            if last_player_status is not PlayerStatus.not_running:
                break
            time.sleep(1)

def stop_playback():
    '''
    Stops playing the current song.
    '''
    subprocess.run(['cmus-remote','--stop'])

def resume_playback(song_position, resume_delay):
    '''
    Starts playing the next song in the playlist.
    Parameters:
        song_position (str):  the position in the song from which to start playing
        resume_delay (number): number of seconds to wait before starting to play
    '''
    if resume_delay is not None and 0 < resume_delay:
        time.sleep(resume_delay)
    subprocess.run(['cmus-remote','--play'])
    if song_position is not None and str(song_position) is not '0':
        subprocess.run(['cmus-remote','--seek', str(song_position)])

def play_next_song():
    '''
    Skip to the next song.
    This will stop playing the current song but will not automatically play the new selected song.
    To start playing the song call resume_playback('0',0) after calling this function
    '''
    subprocess.run(['cmus-remote','--next'])

def play_previous_song():
    '''
    Skip to the previous song.
    This will stop playing the current song but will not automatically play the new selected song.
    To start playing the song call resume_playback('0',0) after calling this function
    '''
    subprocess.run(['cmus-remote','--prev'])



